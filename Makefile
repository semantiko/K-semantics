#FOLDER="IMP"
#PATH=$(cat pwd)/$(FOLDER)

#generate:
#	ocamlc -o

#kompile:
#	kompile $(FILE)

#krun:
#	krun $(FILE) --directory $(PATH)

#kprove:
#	kprove $(FILE) --directory $(PATH)

clean:
	#rm -rf *~
	rm -rf toy_semantics/*/*-kompiled/
	rm -rf toy_semantics/*/*-semantics/
	rm -rf real_semantics/*/*-kompiled/
	rm -rf real_semantics/*/*-semantics/
