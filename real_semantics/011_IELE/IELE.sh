#! /bin/bash -e

sem=iele
build_path=.build/usr/lib/kevm # This repository generates the build-products
                               # for each backend in it.

# This script generates the Kore file associated to the K semantic of EVM.

cyanfonce='\e[0;36m'
neutre='\e[0;m'


rm -rf $sem-semantics

echo -e "${cyanfonce}Cloning of the current semantic:${neutre}"
git clone https://github.com/runtimeverification/$sem-semantics.git
cd $sem-semantics


sudo apt install ./kiele_0.1.0_amd64_bionic.deb

echo -e "${cyanfonce}Compilation of the semantic:${neutre}"
make build # ?

echo -en "${cyanfonce}Copying of the Kore file: ${neutre}"
mv $build_path/$sem-kompiled/definition.kore ../$sem-semantics.kore
echo -e "DONE"

echo -en "${cyanfonce}Deletion of the semantic: ${neutre}"
cd ../
rm -rf $sem-semantics
echo -e "DONE"
