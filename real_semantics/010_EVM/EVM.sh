#! /bin/bash -e

sem=evm
build_path=.build/usr/lib/kevm # This repository generates the build-products
                               # for each backend in it.

# This script generates the Kore file associated to the K semantic of Ethereum Virtual Machine.

cyanfonce='\e[0;36m'
neutre='\e[0;m'


rm -rf $sem-semantics

echo -e "${cyanfonce}Cloning of the current semantic:${neutre}"
git clone https://github.com/runtimeverification/$sem-semantics.git
cd $sem-semantics

echo -e "${cyanfonce}Installing of semantic dependencies:${neutre}"
sudo apt-get install --yes                                                             \
            autoconf bison clang-10 cmake curl flex gcc jq libboost-test-dev           \
            libcrypto++-dev libffi-dev libgflags-dev libjemalloc-dev libmpfr-dev       \
            libprocps-dev libsecp256k1-dev libssl-dev libtool libyaml-dev lld-10       \
            llvm-10-tools make maven netcat-openbsd openjdk-11-jdk pkg-config          \
            libprotobuf-dev protobuf-compiler python3 python-pygments rapidjson-dev time zlib1g-dev

echo -e "${cyanfonce}Installing of Z3:${neutre}"
git clone https://github.com/Z3Prover/z3.git
cd z3
git checkout z3-4.8.11
python scripts/mk_make.py
cd build
make
sudo make install

cd ../..

echo -e "${cyanfonce}Installing of stack:${neutre}"
sudo wget -qO- https://get.haskellstack.org/ | sh
stack upgrade
export PATH=$HOME/.local/bin:$PATH


echo -e "${cyanfonce}Installing of K:${neutre}"
git submodule update --init --recursive -- deps/k
make k-deps

echo -e "${cyanfonce}Installing of blockchain plugin:${neutre}"
git submodule update --init --recursive -- deps/plugin
make plugin-deps


echo -e "${cyanfonce}Compilation of the semantic:${neutre}"
make build

echo -en "${cyanfonce}Copying of the Kore file: ${neutre}"
mv $build_path/llvm/driver-kompiled/definition.kore    ../$sem-LLVM.kore
mv $build_path/haskell/driver-kompiled/definition.kore ../$sem-Haskell.kore
mv $build_path/node/evm-node-kompiled/definition.kore  ../$sem-node.kore
echo -e "DONE"

echo -en "${cyanfonce}Deletion of the semantic: ${neutre}"
cd ../
rm -rf $sem-semantics
echo -e "DONE"
