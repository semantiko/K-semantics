#! /bin/bash -e

sem=evm
build_path=.build/usr/lib/keei # This repository generates the build-products
                               # for each backend in it.

# This script generates the Kore file associated to the K semantic of Ethereum Virtual Machine.

cyanfonce='\e[0;36m'
neutre='\e[0;m'


rm -rf $sem-semantics

echo -e "${cyanfonce}Installing of semantic dependencies:${neutre}"
sudo apt-get install --yes                             \
     make gcc maven openjdk-8-jdk flex opam pkg-config \
     libmpfr-dev autoconf libtool pandoc zlib1g-dev
sudo apt-get install --yes z3 libz3-dev

echo -e "${cyanfonce}Cloning of the current semantic:${neutre}"
git clone https://github.com/runtimeverification/$sem-semantics.git
cd $sem-semantics

echo -e "${cyanfonce}Compilation of the semantic:${neutre}"
make deps
make build

echo -en "${cyanfonce}Copying of the Kore file: ${neutre}"
mv $build_path/$sem-kompiled/definition.kore ../$sem-semantics.kore
echo -e "DONE"

echo -en "${cyanfonce}Deletion of the semantic: ${neutre}"
cd ../
rm -rf $sem-semantics
echo -e "DONE"
