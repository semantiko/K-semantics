#! /bin/bash -e

sem=solidity
build_path=.build/defn # This repository generates the build-products
                       # for each backend in it.

# This script generates the Kore file associated to the K semantic of Solidity.

cyanfonce='\e[0;36m'
neutre='\e[0;m'

rm -rf $sem-semantics

echo -e "${cyanfonce}Cloning of the current semantic:${neutre}"
git clone https://github.com/kframework/$sem-semantics.git
cd $sem-semantics

echo -e "${cyanfonce}Installing of semantic dependencies:${neutre}"
sudo apt install                                                          \
         autoconf curl flex gcc libffi-dev libmpfr-dev libtool make maven \
         opam openjdk-8-jdk pandoc pkg-config python3 python-pygments     \
         python-recommonmark python-sphinx time zlib1g-dev

git submodule update --init --recursive

# If you haven't already setup K's OCaml dependencies more recently than February 1, 2019, then you also need to setup the K OCaml dependencies:

./.build/k/k-distribution/src/main/scripts/bin/k-configure-opam-dev

# NOTE: It may prove useful to first do rm -rf ~/.opam if you've setup K projcets in the past and are experiencing trouble with the newest opam libraries. This is a fairly destructive operation, and will break any other projects that depend on specific locally installed ocaml packages.



echo -e "${cyanfonce}Compilation of the semantic:${neutre}"
make deps
make build
# To only build specific backends, you can do make build-java, make build-ocaml, or make build-haskell.
echo -e "DONE"

echo -en "${cyanfonce}Copying of the Kore file: ${neutre}"
mv $build_path/java/definition.kore ../$sem-semantics-java.kore
mv $build_path/ocaml/definition.kore ../$sem-semantics-ocaml.kore
echo -e "DONE"

echo -en "${cyanfonce}Deletion of the semantic: ${neutre}"
cd ../
rm -rf $sem-semantics
echo -e "DONE"
