#! /bin/bash -e

# This script generates the Kore file associated to the K semantic of Michelson.

# Thanks to Stephen Skeirik for their explanations !

cyanfonce='\e[0;36m'
neutre='\e[0;m'

# So, we have several versions of the semantics.
# The easiest one to use is probably the "prove" version. To build it, either have a recent enough copy of K installed locally on your machine (or) do git submodule update --init --recursive and make deps-k to build the included K submodule and then to build the prove semantics, run: make build-prove.
# The resulting kompiled files should be in .build/defn/prove relative to the toplevel of the Git repo.

rm -rf michelson-semantics

echo -e "${cyanfonce}Cloning of the current semantic:${neutre}"
git clone https://github.com/runtimeverification/michelson-semantics.git
cd michelson-semantics

echo -e "${cyanfonce}Installing of K:${neutre}"
git submodule update --init --recursive
make deps-k

echo -e "${cyanfonce}Installing of semantic dependencies:${neutre}"
# See INSTALL.md to update this part
make deps

echo -e "${cyanfonce}Compilation of the semantic:${neutre}"
make build-prove
make build-llvm

echo -en "${cyanfonce}Copying of the Kore file: ${neutre}"
mv .build/defn/prove/michelson-kompiled/definition.kore ../michelson-semantics-prove.kore
mv .build/defn/llvm/michelson-kompiled/definition.kore  ../michelson-semantics-llvm.kore
echo -e "DONE"

echo -en "${cyanfonce}Deletion of the semantic: ${neutre}"
cd ../
rm -rf michelson-semantics
echo -e "DONE"

# So, there are different versions of the semantics in the repository depending on which compile time flags are used.
# You'll note that this code block is annotated with the symbolic tag. I think there are four tags:
#   k - denotes code that is always used
#   symbolic - denotes code that is used for the symbolic version of the semantics
#   internalized-rl - denotes code that is only used for the semantics that supports the symbolic unit test format
#   concrete - denotes code that is only used when executing programs concretely
# You'll probably want to the symbolic version because it will be a bit more friendly for your purposes (usage in a theorem prover like dedukti). However, it does have these existential variables; however, these variables are only used for type checking, if I recall correctly, so you may be able to just remove them and all usages of them.
# I am not sure if these details about the tags are 100% correct, but hopefully that can point you in the right direction.
