#! /bin/bash -e

sem=javacard

# This script generates the Kore file associated to the K semantic of Modelink.

cyanfonce='\e[0;36m'
neutre='\e[0;m'

rm -rf $sem-semantics

echo -e "${cyanfonce}Cloning of the current semantic:${neutre}"
git clone https://github.com/kframework/$sem-semantics.git
cd $sem-semantics

echo -en "${cyanfonce}Compilation of the semantic:${neutre}"
kompile jcard.k
echo -e "DONE"

echo -en "${cyanfonce}Copying of the Kore file: ${neutre}"
mv jcard-kompiled/definition.kore ../../../$sem-semantics.kore
echo -e "DONE"

echo -en "${cyanfonce}Deletion of the semantic: ${neutre}"
cd ../
rm -rf $sem-semantics
echo -e "DONE"
