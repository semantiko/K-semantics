#! /bin/bash -e

sem=hybrid-programs

# This script generates the Kore file associated to the K semantic of Hybrid programs.

cyanfonce='\e[0;36m'
neutre='\e[0;m'

rm -rf $sem-semantics

echo -e "${cyanfonce}Cloning of the current semantic:${neutre}"
git clone https://github.com/Formal-Systems-Laboratory/$sem-semantics.git
cd $sem-semantics

echo -en "${cyanfonce}Compilation of the semantic:${neutre}"
sed -i "s/\`\`\`//g" *.md
sed -i "s/{.k}//g" *.md
sed -i "s/.k/.md/g" *.md
echo -en "${cyanfonce}...khp-real${neutre}"
kompile khp-real.md --main-module REAL
echo -en "${cyanfonce}...wolframlanguage${neutre}"
kompile wolframlanguage.md
echo -en "${cyanfonce}...khp${neutre}"
kompile khp.md
echo -e "DONE"

echo -en "${cyanfonce}Copying of the Kore file: ${neutre}"
mv khp-kompiled/definition.kore ../$sem-semantics.kore
echo -e "DONE"

echo -en "${cyanfonce}Deletion of the semantic: ${neutre}"
cd ../
rm -rf $sem-semantics
echo -e "DONE"
