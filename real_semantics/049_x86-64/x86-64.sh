#! /bin/bash -e

sem=X86-64

# This script generates the Kore file associated to the K semantic of x86-64.

cyanfonce='\e[0;36m'
neutre='\e[0;m'

rm -rf $sem-semantics

echo -e "${cyanfonce}Installing of the specific K version:${neutre}"
wget https://github.com/runtimeverification/k/releases/download/v5.0.0/kframework_5.0.0_amd64_bionic.deb
sudo apt-get install -y ./kframework_5.0.0_amd64_bionic.deb

echo -e "${cyanfonce}Cloning of the current semantic:${neutre}"
git clone https://github.com/kframework/$sem-semantics.git
cd $sem-semantics/semantics

echo -en "${cyanfonce}Compilation of the semantic:${neutre}"
../scripts/kompile.pl --backend java
echo -e "DONE"

echo -en "${cyanfonce}Copying of the Kore file: ${neutre}"
mv $sem-kompiled/definition.kore ../$sem-semantics.kore
echo -e "DONE"

echo -en "${cyanfonce}Deletion of the semantic: ${neutre}"
cd ../
rm -rf $sem-semantics
echo -e "DONE"
