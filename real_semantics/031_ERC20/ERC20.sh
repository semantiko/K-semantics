#! /bin/bash -e

# This script generates the Kore file associated to the K semantic of ERC20.

sem=erc20

cyanfonce='\e[0;36m'
neutre='\e[0;m'

rm -rf $sem-semantics

echo -e "${cyanfonce}Cloning of the current semantic:${neutre}"
git clone https://github.com/runtimeverification/$sem-semantics
cd $sem-semantics

echo -e "${cyanfonce}Compilation of the semantic:${neutre}"
kompile $sem.md
echo -e "DONE"

echo -en "${cyanfonce}Copying of the Kore file: ${neutre}"
mv $sem-kompiled/definition.kore ../$sem.kore
echo -e "DONE"

echo -en "${cyanfonce}Deletion of the semantic: ${neutre}"
cd ../
rm -rf $sem-semantics
echo -e "DONE"
