#! /bin/bash -e

sem=java

# This script generates the Kore file associated to the K semantic of OCaml.

cyanfonce='\e[0;36m'
neutre='\e[0;m'

rm -rf $sem-semantics

echo -e "${cyanfonce}Cloning of the current semantic:${neutre}"
git clone --depth=1 https://github.com/kframework/$sem-semantics.git
chmod +x $sem-semantics/tools/*
export PATH=$PATH:~/$sem-semantics/tools

echo -en "${cyanfonce}Compilation of the semantic:${neutre}"
cd $sem-semantics/src
kompile -v --debug -d exec exec/java-exec.k
kompile -v --debug -d prep prep/java-prep.k
echo -e "DONE"

echo -en "${cyanfonce}Copying of the Kore file: ${neutre}"
mv $sem-kompiled/definition.kore ../../../$sem-semantics.kore
echo -e "DONE"

echo -en "${cyanfonce}Deletion of the semantic: ${neutre}"
cd ../
rm -rf $sem-semantics
echo -e "DONE"
