#! /bin/bash -e

# This script generates the Kore file associated to the K semantic of Elrond.

sem=elrond

cyanfonce='\e[0;36m'
neutre='\e[0;m'


rm -rf $sem-semantics

echo -e "${cyanfonce}Cloning of the current semantic:${neutre}"
git clone https://github.com/runtimeverification/$sem-semantics
cd $sem-semantics

echo -e "${cyanfonce}Installing of semantic dependencies:${neutre}"
make $sem-deps

echo -e "${cyanfonce}Compilation of the semantic:${neutre}"
#make build

echo -en "${cyanfonce}Copying of the Kore file: ${neutre}"
#mv .build/defn/haskell/wasm-kompiled/definition.kore ../wasm-semantics-haskell.kore
#mv .build/defn/llvm/wasm-kompiled/definition.kore    ../wasm-semantics-llvm.kore
echo -e "DONE"

echo -en "${cyanfonce}Deletion of the semantic: ${neutre}"
cd ../
rm -rf $sem-semantics
echo -e "DONE"
