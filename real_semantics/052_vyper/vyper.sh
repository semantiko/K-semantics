#! /bin/bash -e

sem=vyper

# This script generates the Kore file associated to the K semantic of Vyper.

cyanfonce='\e[0;36m'
neutre='\e[0;m'

rm -rf $sem-semantics


echo -e "${cyanfonce}Cloning of the current semantic:${neutre}"
git clone https://github.com/kframework/$sem-semantics.git
cd $sem-semantics

echo -e "${cyanfonce}Installing of a specific K version:${neutre}"
git submodule update --init k
cd k
mvn package -DskipTests

export PATH="`pwd`/k-distribution/target/release/k/bin:$PATH"


echo -en "${cyanfonce}Compilation of the semantic:${neutre}"
kompile --syntax-module VYPER-ABSTRACT-SYNTAX vyper-lll/vyper-lll-post.k
kompile --syntax-module LLL-EVM-INTERFACE     lll-evm/lll-evm.k
echo -e "DONE"

echo -en "${cyanfonce}Copying of the Kore file: ${neutre}"
mv $sem-kompiled/definition.kore ../$sem-semantics.kore
echo -e "DONE"

echo -en "${cyanfonce}Deletion of the semantic: ${neutre}"
cd ../
rm -rf $sem-semantics
echo -e "DONE"
