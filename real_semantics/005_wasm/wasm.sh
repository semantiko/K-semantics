#! /bin/bash -e

# This script generates the Kore file associated to the K semantic of WebAssembly.

cyanfonce='\e[0;36m'
neutre='\e[0;m'


rm -rf wasm-semantics

echo -e "${cyanfonce}Cloning of the current semantic:${neutre}"
git clone https://github.com/runtimeverification/wasm-semantics.git
cd wasm-semantics

echo -e "${cyanfonce}Installing of K:${neutre}"
git submodule update --init --recursive
make deps-k

echo -e "${cyanfonce}Installing of semantic dependencies:${neutre}"
sudo apt-get install --yes                                                            \
    autoconf bison clang++-8 clang-8 cmake curl flex gcc libboost-test-dev libffi-dev \
    libgmp-dev libjemalloc-dev libmpfr-dev libprocps-dev libprotobuf-dev libtool      \
    libyaml-dev lld-8 llvm llvm-8 llvm-8-tools maven openjdk-8-jdk pandoc pkg-config  \
    protobuf-compiler python3 python-pygments python-recommonmark python-sphinx time  \
    zlib1g-dev

# To upgrade stack (if needed):
stack upgrade
export PATH=$HOME/.local/bin:$PATH

git submodule update --init --recursive
make deps

echo -e "${cyanfonce}Installing of Z3:${neutre}"
git clone https://github.com/Z3Prover/z3.git
cd z3
git checkout z3-4.8.11
python scripts/mk_make.py
cd build
make
sudo make install

echo -e "${cyanfonce}Compilation of the semantic:${neutre}"
make build
# Or:
# make build-haskell
# make build-llvm

echo -en "${cyanfonce}Copying of the Kore file: ${neutre}"
mv .build/defn/haskell/wasm-kompiled/definition.kore ../wasm-semantics-haskell.kore
mv .build/defn/llvm/wasm-kompiled/definition.kore    ../wasm-semantics-llvm.kore
echo -e "DONE"

echo -en "${cyanfonce}Deletion of the semantic: ${neutre}"
cd ../
rm -rf wasm-semantics
echo -e "DONE"
