#! /bin/bash -e

sem=c
build_path=.build/defn # This repository generates the build-products
                       # for each backend in it.

# This script generates the Kore file associated to the K semantic of C.

cyanfonce='\e[0;36m'
neutre='\e[0;m'

rm -rf $sem-semantics


# On Ubuntu 18.04, the installation process for our C semantics can be summarized as:

echo -e "${cyanfonce}Installing of semantic dependencies:${neutre}"
sudo apt-get install --yes \
  maven git openjdk-8-jdk flex libgmp-dev libffi-dev \
  libmpfr-dev build-essential cmake zlib1g-dev \
  diffutils libuuid-tiny-perl \
  libstring-escape-perl libstring-shellquote-perl \
  libgetopt-declare-perl pkg-config \
  libapp-fatpacker-perl liblocal-lib-perl \
  clang-6.0 libclang-6.0-dev

# opam switch --help
# opam switch install 4.09.0
eval $(opam env)

echo -e "${cyanfonce}Cloning of the current semantic:${neutre}"
git clone https://github.com/kframework/$sem-semantics.git
cd $sem-semantics

echo -e "${cyanfonce}Compilation of the semantic:${neutre}"
git submodule update --init --recursive
eval $(opam config env)
eval $(perl -I "~/perl5/lib/perl5" -Mlocal::lib)
make -j4 --output-sync=line


echo -en "${cyanfonce}Copying of the Kore file: ${neutre}"
ls -a
ls .build
ls $build_path
ls $build_path/$sem-kompiled/
mv $build_path/$sem-kompiled/definition.kore ../$sem-semantics.kore
echo -e "DONE"

echo -en "${cyanfonce}Deletion of the semantic: ${neutre}"
cd ../
rm -rf $sem-semantics
echo -e "DONE"
