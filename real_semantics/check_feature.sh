#! /bin/bash -e

# Some colors
noir='\e[0;30m'
gris='\e[1;30m'
rougefonce='\e[0;31m'
rose='\e[1;31m'
vertfonce='\e[0;32m'
vertclair='\e[1;32m'
orange='\e[0;33m'
jaune='\e[1;33m'
bleufonce='\e[0;34m'
bleuclair='\e[1;34m'
violetfonce='\e[0;35m'
violetclair='\e[1;35m'
cyanfonce='\e[0;36m'
cyanclair='\e[1;36m'
grisclair='\e[0;37m'
blanc='\e[1;37m'
neutre='\e[0;m'

if [[ $# -ne 1 ]]; then
    echo -e "${rougefonce}ERROR: Too many/few arguments.
       Expecting one to specify the path where running the script.${neutre}"
    exit 1
fi

function count() {
  local pattern="$1"
  local filename="$2"
  #echo "res1" "$pattern"
  #echo "res2" "$filename"
  eval grep \"$pattern\" $filename | wc -l
}

function print() {
    if [ "$2" -gt 0 ]; then
      echo -e "$1" "is present:" "$2"
    fi
}

for f in $(find "$1" -type f); do
  #echo $sem_dir
  #for f in $sem_dir; do

  filename=$(echo ${f##*/})
  if [ $filename = "README.md" ] || [ $filename = "INSTALL.md" ] \
    || [ $filename = "CONTRIBUTING.md" ] || [ $filename = "issues.md" ];then
    continue
  fi

  extension=$(echo ${f##*.})
  if [ $extension = "md" ] || [ $extension = "k" ]; then
    #############
    # STEP 1: Delete ```k ... ``` and comment
    #############
    nb_balise=$(grep "\`\`\`k" $f | wc -l)
    if [ $nb_balise -gt 0 ]; then
      sed -n "/\`\`\`k/,/\`\`\`/p" $f | sed /\`\`\`k/d | sed /\`\`\`/d > tmp
    else
      cat $f > tmp
    fi
    sed '/^\/\//d' tmp > tmp2
    sed 's://.*$::g' tmp2 > tmp3
    perl -0777pe 's#/\*.*?\*/##gms' tmp3 > tmp
    if [ -s tmp ]; then

      echo -e "${cyanfonce}Check the file" $f ":${neutre}"
    #tmp=$(cat tmp)
    #echo -e $tmp
      tmp="tmp"

    #############
    # STEP 2: Count the number of each attribute, etc.
    #############

    # Attribute about binder
    nb_binder=$(count "binder" $tmp)
    # Attribute about evaluation stratagies
    nb_hybrid=$(count "hybrid" $tmp)
    nb_result=$(count "result" $tmp)
    # Attribute about rewriting
    nb_priority=$(count "priority(" $tmp)
    nb_idem=$(count "idem" $tmp)
    nb_comm=$(($(count "comm" $tmp) - $(count "command" $tmp)))
    nb_asso=$(($(count "asso" $tmp) - $(count "non-assoc" $tmp)))
    nb_unit=$(count "unit" $tmp)
    #
    nb_var_exist=$(count "?" $tmp)
    nb_question_mark=$(grep "\"?" $tmp | wc -l)
    # Attribute about configuration
    nb_multiplicity=$(count "multiplicity" $tmp)
    nb_type=$(count "type" $tmp)
    nb_exit=$(count "exit" $tmp)
    # Attribute about
    nb_freshGenerator=$(count "freshGenerator" $tmp)
    nb_var_bang=$(count "!" $tmp)
    nb_bang_not=$(grep "\"!\"" $tmp | wc -l)
    nb_bang_equal=$(grep "\"!=\"" $tmp | wc -l)
    nb_memo=$(($(count "memo" $tmp) - $(count "memory" $tmp)))

    #############
    # STEP 3: Print the summarize
    #############

    print "The attribute binder"   $nb_binder
    print "The attribute hybrid"   $nb_hybrid
    print "The attribute result"   $nb_result
    print "The attribute priority" $nb_priority
    print "The attribute idem" $nb_idem
    print "The attribute comm" $nb_comm
    print "The attribute asso" $nb_asso
    print "The attribute unit" $nb_unit

    print "Existentiel variable (?)" $nb_var_exist
    if [ $nb_question_mark -gt 0 ]; then
      echo -e "${jaune}WARNING: The terminal \"? is present in the syntax.${neutre}"
    fi

    print "The attribute multiplicity" $nb_multiplicity
    print "The attribute type" $nb_type
    print "The attribute exit" $nb_exit
    print "The attribute freshGenerator" $nb_freshGenerator
    print "Fresh variable (!)" $nb_var_bang
    if [ $nb_bang_not -gt 0 ]; then
      echo -e "${jaune}WARNING: The terminal \"!\" is present in the syntax.${neutre}"
    fi
    if [ $nb_bang_equal -gt 0 ]; then
      echo -e "${jaune}WARNING: The terminal \"!=\" is present in the syntax.${neutre}"
    fi

    print "The attribute memo" $nb_memo
    fi
    rm tmp tmp2 tmp3
  fi
done
