#! /bin/bash -e

sem=plutus-core
build_path=.build/usr/lib/ # This repository generates the build-products
                           # for each backend in it.

# This script generates the Kore file associated to the K semantic of Plutus.

cyanfonce='\e[0;36m'
neutre='\e[0;m'


rm -rf $sem-semantics

echo -e "${cyanfonce}Cloning of the current semantic:${neutre}"
git clone https://github.com/runtimeverification/$sem-semantics.git
cd $sem-semantics
echo -e "DONE"

echo -e "${cyanfonce}Installing of semantic dependencies:${neutre}"
git submodule update --init --recursive
make deps RELEASE=true
echo -e "DONE"


echo -e "${cyanfonce}Compilation of the semantic:${neutre}"
make build -j8 # Build everything
# Build LLVM backend: make build-llvm -j8
# Build Haskell backend: make build-haskell -j8
# Build just KPlutus runner and includes: make build-kplutus -j8
echo -e "DONE"

echo -en "${cyanfonce}Copying of the Kore file: ${neutre}"
mv $build_path/kplutus/llvm/uplc-kompiled/definition.kore    ../$sem-LLVM.kore
mv $build_path/kplutus/haskell/uplc-kompiled/definition.kore ../$sem-Haskell.kore
echo -e "DONE"

echo -en "${cyanfonce}Deletion of the semantic: ${neutre}"
cd ../
rm -rf $sem-semantics
echo -e "DONE"
