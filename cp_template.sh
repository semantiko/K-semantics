#!/bin/bash

pwdbis=`pwd`

# Pour chaque dossier (qui contient un langage)
for path in `ls`
do
     # echo $path
     if [ -d $path ]; then
        # echo $path
        cp krun.sh .krun
        chmod +x .krun
        #currPWD=`pwd`\/$path
        sed -i "s|@PWD|$pwdbis/$path|" .krun
        cp kprove.sh .kprove
        chmod +x .kprove
        sed -i "s|@PWD|$pwdbis/$path|" .kprove
        # echo `ls -a`
        # Nous rajoutons un fichier .krun et .kprove
        for folder in `find . -mindepth 1 -type d`
        do
             # echo $folder
             cp .krun $folder
             cp .kprove $folder
        done
        rm .krun .kprove
     fi
done
