This project gathers various semantics of programming languages written in
K, as well as a series of example programs for each language.
Each folder corresponds to a unique programming language, and suggests
examples of programs and their specifications.


The content of the folder real_semantics/ is the following:

|               Semantic               |              Contact                 | Possible to generate a Kore file? | Possible to translate into Dedukti? | 
| :----------------------------------- |:------------------------------------:| :-------------------------------: | :---------------------------------: |
| WebAssembly                          |            Jenkins ?                 |              yes                  |    no (running for a long time)     |
| Ethereum Virtual Machine (EVM)       |      Jenkins/Hildenbrandt            |              yes                  |              yes                    |
| IELE                                 |            Jenkins ?                 |                                   |                                     |
| Michelson                            |        Jenkins/Sheirik               |              yes                  |              yes                    |
| Elrond                               |         Jenkins/Zhang                |              no                   |               -                     |
| C                                    |        Ellison/Hathhorn              |              no                   |               -                     |
| P4                                   |           Kheradmand                 |              yes                  |    no (running for a long time)     |
| LLVM                                 |             Liyi Li (last)           |              no                   |               -                     |
| Python                               |         Dwight Guth (last)           |              no                   |               -                     |
| Java                                 |      Bogdanas/Yuwen (last)           |              no                   |               -                     |
| OCaml                                |               Lazar (last)           |              no                   |               -                     |
| JavaScript                           |                Park (last)           |              no                   |               -                     |
| AADL                                 |                 Liu (last)           |              no                   |               -                     |
| Alk                                  |              Lucanu (last)           |              no                   |               -                     |
| Cink                                 |              Lucanu (last)           |              no                   |               -                     |
| jvm                                  |             Onofrei (last)           |              no                   |               -                     |
| Modelink                             |            Arusoaie (last)           |              no                   |               -                     |
| JavaCard                             |        "irina-naum" (last)           |              no                   |               -                     |
| Orc                                  |             Duhaiby (last)           |              no                   |               -                     |
| Haskell core                         |               Tosun (last)           |              no                   |               -                     |
| Plutus core                          |        Kaneda/Braga (last)           |                                   |                                     |
| x86-64                               |            Dasgupta (last)           |              no                   |               -                     |
| Vyper                                |      Bogdanas/Zhang (last)           |                                   |                                     |
| ERC20                                |                Rosu (last)           |                                   |                                     |
| Solidity                             |           Filaretti (last)           |                                   |                                     |
| Ethereum Environment Interface (EEI) |               Hjort (last)           |                                   |                                     |
| ERC777                               |            Bogdanas (last)           |                                   |                                     |
| K                                    |      Nishant/Saxena (last)           |                                   |                                     |
| Yul                                  |           "MrChico" (last)           |              no (acces problem)   |               -                     |
| KEwasm                               |   Hjort/Jenkins/Hildenbrandt (last)  |              no                   |               -                     |
| hybrid programs                      |              Saxena (last)           |              no (almost?)         |               -                     |
| Boogie                               |     Nishant/Poulsen (last)           |              no (almost?)         |               -                     |


The below list of semantics can be found at https://kframework.org/projects/ too.

* Featured
   - KWasm (Aug 2015 - Present): 
      https://github.com/runtimeverification/wasm-semantics
   - KEVM (Sep 2017 - Present):
      https://github.com/runtimeverification/evm-semantics
   - IELE (Oct 2016 - Present):
      https://github.com/runtimeverification/iele-semantics
   - K-Michelson (Oct 2019 - Present): 
      https://github.com/runtimeverification/michelson-semantics
   - Elrond (July 2020 - Present):
      https://github.com/runtimeverification/elrond-semantics
   - C (Jul 2010 - Present):
      https://github.com/kframework/c-semantics
   - P4 (Nov 2016 - Present):
      https://github.com/kframework/p4-semantics

* Archived
   - llvm (2011-2018):
      https://github.com/kframework/llvm-semantics
   - python (2012-2013):
      https://github.com/kframework/python-semantics
   - java (2012-2016):
      https://github.com/kframework/java-semantics
   - ocaml (2012-2013):
      https://github.com/kframework/ocaml-semantics
   - javascript (2013-2015):
      https://github.com/kframework/javascript-semantics
   - aadl (2013-2013):
      https://github.com/kframework/aadl-semantics
   - alk (2013-2014):
      https://github.com/kframework/alk-semantics
   - cink (2013-2015):
      https://github.com/kframework/cink-semantics
   - jvm (2013-2014):
      https://github.com/kframework/jvm-semantics
   - modelink (2013-2013):
      https://github.com/kframework/modelink-semantics
   - javacard (2014-2014):
      https://github.com/kframework/javacard-semantics
   - orc (2016-2017):
      https://github.com/kframework/orc-semantics
   - haskell core (2016-2017):
      https://github.com/kframework/haskell-core-semantics
   - plutus core (2016-2019):
      https://github.com/runtimeverification/plutus-core-semantics
   - X86-64 (2017-2020):
      https://github.com/kframework/X86-64-semantics
   - vyper (2017-2018):
      https://github.com/kframework/vyper-semantics
   - erc20 (2017-2018)
      https://github.com/runtimeverification/erc20-semantics
   - solidity (2018-2019)
      https://github.com/runtimeverification/solidity-semantics
   - eei (2018-2019):
      https://github.com/kframework/eei-semantics
   - erc777 (2018-2018):
      https://github.com/runtimeverification/erc777-semantics
   - k (2018-2020):
      https://github.com/kframework/k-in-k
   - yul (2019-2019):
      https://github.com/ethereum/Yul-K
   - KEwasm (2019-2020):
      https://github.com/kframework/ewasm-semantics
   - hybrid programs (2020-2020):
      https://github.com/Formal-Systems-Laboratory/hybrid-programs-semantics
   - Boogie (2020):
      https://github.com/kframework/boogie-semantics
